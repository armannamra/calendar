<?php
/**
 * Created by IntelliJ IDEA.
 * User: Arman
 * Date: 9/12/15
 * Time: 1:41 AM
 * To change this template use File | Settings | File Templates.
 */

class EventsController extends BaseController {

    public function getEvents(){

        $events = Events::all()->toJson();
        return $events;
    }

    public function addEvents(){
        try{
            $event = new Events();
            $data = Input::all();

            $event->title = $data['title'];
            $event->start = $data['start'];
            $event->end = $data['end'];

            $event->save();
            return "Ok";
        } catch (\Exeption $e){

            return $e->getMessage();
        }
    }

    public function updateEvents(){
        try{
            $data = Input::all();
            $event = Events::find($data['id']);

            $event->title = $data['title'];
            $event->start = $data['start'];
            $event->end = $data['end'];

            $event->save();

            return "Ok";
        } catch (\Exeption $e){
            return $e->getMessage();
        }
    }
}