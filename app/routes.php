<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'IndexController@index');
Route::post('/', 'IndexController@index');

Route::get('/events', 'EventsController@getEvents');
Route::post('/add', 'EventsController@addEvents');
Route::post('/update', 'EventsController@updateEvents');


