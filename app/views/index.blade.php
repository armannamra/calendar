<?php
/**
 * Created by IntelliJ IDEA.
 * User: Arman
 * Date: 9/12/15
 * Time: 1:04 AM
 * To change this template use File | Settings | File Templates.
 */
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8' />
    <link href='<?=Config::get('app.assets.css')?>/index.css' rel='stylesheet' />
    <link href='<?=Config::get('app.assets.css')?>/fullcalendar.css' rel='stylesheet' />
    <link href='<?=Config::get('app.assets.css')?>/fullcalendar.print.css' rel='stylesheet' media='print' />
    <script src='<?=Config::get('app.assets.bower')?>/moment/moment.js'></script>
    <script src='<?=Config::get('app.assets.bower')?>/jquery/jquery.min.js'></script>
    <script src='<?=Config::get('app.assets.bower')?>/fullcalendar/dist/fullcalendar.min.js'></script>
    <script src='<?=Config::get('app.assets.js')?>/calendar.js'></script>
</head>
<body>

<div id='calendar'></div>

</body>
</html>
