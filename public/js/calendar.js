/**
 * Created with IntelliJ IDEA.
 * User: Arman
 * Date: 9/12/15
 * Time: 3:27 AM
 * To change this template use File | Settings | File Templates.
 */

$(document).ready(function() {

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    var calendar = $('#calendar').fullCalendar({
        editable: true,
        events: "http://localhost/events",

        selectable: true,
        selectHelper: true,
        select: function(start, end, allDay) {
            var title = prompt('Event Title:');
            if (title) {
                var start = moment(start).format('YYYY-MM-DDTHH:mm:ssZ');
                var end = moment(end).format('YYYY-MM-DDTHH:mm:ssZ');
                $.ajax({
                    url: 'http://localhost/add',
                    data: 'title='+ title+'&start='+ start +'&end='+ end ,
                    type: "POST",
                    success: function(json) {
                        alert('OK');
                    }
                });
                calendar.fullCalendar('renderEvent',
                    {
                        title: title,
                        start: start,
                        end: end,
                        allDay: allDay
                    },
                    true // make the event "stick"
                );
            }
            calendar.fullCalendar('unselect');
        },
        eventDrop: function(event, delta) {
            var start = moment(event.start).format('YYYY-MM-DDTHH:mm:ssZ');
            var end = moment(event.end).format('YYYY-MM-DDTHH:mm:ssZ');
            $.ajax({
                url: '/update',
                data: 'title='+ event.title+'&start='+ start +'&end='+ end +'&id='+ event.id ,
                type: "POST",
                success: function(json) {
                    alert("OK");
                }
            });
        },
        eventResize: function(event) {
            var start = moment(event.start).format('YYYY-MM-DDTHH:mm:ssZ');
            var end = moment(event.end).format('YYYY-MM-DDTHH:mm:ssZ');
            $.ajax({
                url: '/update',
                data: 'title='+ event.title+'&start='+ start +'&end='+ end +'&id='+ event.id ,
                type: "POST",
                success: function(json) {
                    alert("OK");
                }
            });

        }
    });

});